package dam.jess;

import java.io.Serializable;
import java.util.ArrayList;

public class Classe implements Serializable{
	
	

	
	String aula;
	String professor;
	ArrayList<Alumnes> als = new ArrayList<Alumnes>();
	
	public Classe (String aula, String professor,ArrayList<Alumnes> guardaAlumnos) {
		this.aula = aula;
		this.professor = professor;
		this.als = guardaAlumnos;
	}
	
	public String getAula() {
		return aula;
	}
	public void setAula(String aula) {
		this.aula = aula;
	}
	public String getProfessor() {
		return professor;
	}
	public void setProfessor(String professor) {
		this.professor = professor;
	}
	public ArrayList<Alumnes> getAlsi() {
		return als;
	}
	public void setAlsi(ArrayList<Alumnes> alsi, ArrayList<Alumnes> als) {
		this.als = als;
	}
	
	public String toString() {
		return "Aula:"+this.aula+"\n"+"Profe:"+this.professor+"\n"+"Alumnos:"+this.als;
		
	}
	

}
