package dam.jess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Alumnes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2907238789229574432L;
	
	private String nom;
	private String cognoms;
	private String DNI;
	private String adreca;
	private String mail;
	private ArrayList<String> telf = new ArrayList<String>();
	

	// Obligatorio - Constructor vacio
	
	public Alumnes() {
		super();
	}

	public Alumnes(String nom, String cognoms, String DNI, String adreca, String mail, ArrayList<?> telf) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.DNI = DNI;
		this.adreca = adreca;
		this.mail = mail;
		this.telf = (ArrayList<String>) telf;
	}
	
	public Alumnes(String nom, String cognoms, String DNI, String adreca, String mail) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.DNI = DNI;
		this.adreca = adreca;
		this.mail = mail;
	}

	@XmlElement(name = "nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement(name = "cognoms")
	public String getCognom() {
		return cognoms;
	}

	public void setCognom(String cognom) {
		this.cognoms = cognom;
	}

	@XmlElement(name = "DNI")
	public String getDNI() {
		return DNI;
	}

	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	@XmlElement(name = "adreca")
	public String getAdreca() {
		return adreca;
	}

	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}

	@XmlElement(name = "mail")
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@XmlElementWrapper(name = "telefons")
    @XmlElement(name = "telefon")
    public ArrayList<String> getTelf() {
        return telf;
    }

	public void setTelf(String telf1) {
		this.setTelf(telf1);
	}
	
	public String toString() {
		return "||nombre:"+this.nom+"||apellido:"+this.cognoms+"||DNI:"+this.DNI+"||mail:"+this.mail+"||direccion:"+this.adreca;	
	}


}
