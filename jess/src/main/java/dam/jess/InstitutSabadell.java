package dam.jess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.sql.rowset.spi.XmlWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import org.json.XML;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class InstitutSabadell implements Serializable {
	private static final long serialVersionUID = -8625954558861762572L;
	/**
	 * ACCES A DADES 2 DAM
	 * Entrega Projecte UF1
	 * IMPORTANTE: Esta ejecutado 1 vez el programa, los archivos ya estan modificados según el MAIN
	 * DE ABAJO. El maven me da un error pero no me indica cual, extraño pero cierto. Si hay algun problema
	 * con el .rar esta subido el proyecto a mi github.
	 * --> gitlab:  https://gitlab.com/Jessvali92/proyectouf1ficheros.git
	 *
	 * @author Jessica Valiente 2DAM 
	 * 
	 */
	
	//documents utilitzats
	String proftxt = "professors.txt.txt";
	String archivo = "alumnes.xml";
	String archiJSON = "aules.json";	
	
	
	//metode llegir JSON per practicar
	public void llegirJSON(String archiJSON) {
	
		JSONParser parser = new JSONParser();

		try {

			JSONArray arrysplit = (JSONArray) parser.parse(new FileReader(archiJSON));

			// = (JSONArray) obj;

			for (Object o : arrysplit) {
				

				JSONObject split = (JSONObject) o;
				JSONArray listMaquines = (JSONArray) split.get("maquines");

				for (Object o2 : listMaquines) {

					JSONObject maquina = (JSONObject) o2;

				}

			}
			

			FileWriter file = new FileWriter("LecturaJSON");

			file.write(arrysplit.toJSONString());
			file.flush();

				

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	//metode llegir XML per practicar
	public ArrayList<String> llegirXmlProfes(String proftxt) throws FileNotFoundException, IOException {

		ArrayList<String> listProfe = new ArrayList<String>();
		String cadena;

		try {
			FileReader f = new FileReader(proftxt);
			BufferedReader b = new BufferedReader(f);
			while ((cadena = b.readLine()) != null) {
				listProfe.add(cadena);
			}

			b.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return listProfe;
	}
	
	//metode afegir Professor,afegeix un profe,els ordena y els torna a col·locar
	public void afegirProfessor(String profe) throws FileNotFoundException, IOException {

		ArrayList<String> listProfe2 = new ArrayList<String>();
		listProfe2 = llegirXmlProfes(proftxt);
		listProfe2.add(profe);
		Collections.sort(listProfe2);
		try {
			FileWriter f = new FileWriter(proftxt);
			BufferedWriter b = new BufferedWriter(f);

			for (String cadena : listProfe2) {
				b.write(cadena);
				b.newLine();
			}
			b.flush();
			b.close();
			f.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//elimina un profe del xml
	public void jubilarProfessor(String profe) throws FileNotFoundException, IOException {

		try {
			File fi = new File(proftxt);
			FileReader fr = new FileReader(fi);
			BufferedReader br = new BufferedReader(fr);
			File f2 = new File("nuevo.txt");
			FileWriter fw = new FileWriter(f2);
			BufferedWriter bw = new BufferedWriter(fw);
			String vacio;
			while (br.ready()) {
				vacio = br.readLine();
				if (!vacio.equals(profe)) {
					bw.write(vacio);
					bw.newLine();
				}
			}
			bw.flush();
			bw.close();
			br.close();
			fr.close();

			fi.delete();
			System.out.println(f2.renameTo(fi));// renombre del fitxer
												
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// afregeix un alumne amb JABX accedint amb les classes creades Alumnes/Institut
	public void afregirAlumne(Alumnes al) throws JAXBException {

		try {
			File file = new File(archivo);
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Institut ies = (Institut) jaxbUnmarshaller.unmarshal(file);

			ies.getAlumnes().add(al);// añadir un nuevo elemento alumno

			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.marshal(ies, new FileOutputStream(archivo));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	//Afregeix un telefon amb JAXB accedint a la clase Institut/Alumnes
	public void afregirTelefons(String dniAlumne, String telefon) throws JAXBException {
		try {
			File file = new File(archivo);
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Institut ies = (Institut) jaxbUnmarshaller.unmarshal(file);

			for (int i = 0; i < ies.getAlumnes().size(); i++) {
				if (ies.getAlumnes().get(i).getDNI().equals(dniAlumne)) {
					ies.getAlumnes().get(i).getTelf().add(telefon);
				} else {
					// buscando otro usuario
				}
			}

			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.marshal(ies, new FileOutputStream(archivo));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	//Elimina un alumne per DNI
	public void alCarrer(String dniAlumne) throws JAXBException {

		try {
			File file = new File(archivo);
			JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Institut ies = (Institut) jaxbUnmarshaller.unmarshal(file);

			for (int i = 0; i < ies.getAlumnes().size(); i++) {
				if (ies.getAlumnes().get(i).getDNI().equals(dniAlumne)) {
					ies.getAlumnes().remove(ies.getAlumnes().get(i));
				} else {
					// buscando otro usuario
				}
			}

			Marshaller marshallerObj = jaxbContext.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshallerObj.marshal(ies, new FileOutputStream(archivo));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	//Afegeix una nova maquina amb JSON
	public void comprarMaquina(String nomAula, String nomMaquina, String processador, boolean grafica) {

		try {
			JSONParser parser = new JSONParser();
			Object aulas = parser.parse(new FileReader(archiJSON));
			JSONArray arrayaulas = (JSONArray) aulas;

			for (Object o : arrayaulas) {

				JSONObject aula = (JSONObject) o;
				if (aula.get("nom").equals(nomAula)) {

					JSONArray maquines = (JSONArray) aula.get("maquines");
					JSONObject maquiNueva = new JSONObject();
					maquiNueva.put("nom", nomMaquina);
					maquiNueva.put("processador", processador);
					maquiNueva.put("grafica", grafica);
					maquines.add(maquiNueva);
				}
			}

			FileWriter file = new FileWriter(archiJSON);

			file.write(arrayaulas.toJSONString());
			file.flush();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * PROBANDO ESCRIBIR NUEVO JSON DESDE 0
	 * 
	 * JSONObject aula = new JSONObject(); JSONArray maquines = new JSONArray();
	 * JSONObject maquina = new JSONObject();
	 * 
	 * aula.put("nom", nomAula); aula.put("maquines", maquines); aula.put("aa1",
	 * "aa"); aula.put("aa2", nomAula);
	 * 
	 * maquina.put("nom", nomMaquina); maquina.put("processador", processador);
	 * maquina.put("grafica", grafica);
	 * 
	 * maquines.add(maquina); aulas.add(aula);
	 * 
	 * try { FileWriter file = new FileWriter("a");
	 * 
	 * file.write(aulas.toJSONString());
	 * 
	 * file.flush(); } catch (IOException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } }
	 */

	/*
	 * canvia en el fitxer json la màquina amb el nom proporcionat, l’esborra de la
	 * seva aula, i l’afegeix a l’aula proporcionada
	 */
	
	//canvia un objecte maquina a una altre

	public void canviaMaquina(String nomMaquina, String nomiAula) {
		ArrayList<JSONObject> guardaM = new ArrayList<JSONObject>();
		try {

			JSONParser parser = new JSONParser();
			Object aulas = parser.parse(new FileReader(archiJSON));
			JSONArray arrayaulas = (JSONArray) aulas;

			for (Object o : arrayaulas) {
				JSONObject aula = (JSONObject) o;
				JSONObject marcada = null;
				JSONArray maquines = (JSONArray) aula.get("maquines");
				for (Object o2 : maquines) {
					JSONObject maquina = (JSONObject) o2;
					if (maquina.get("nom").equals(nomMaquina)) {

						guardaM.add(maquina);
						marcada = maquina;
					}
				}
				if (marcada != null) {
					// nos la cascamos
					maquines.remove(marcada);
				}
			}

			for (Object o : arrayaulas) {
				JSONObject aula = (JSONObject) o;
				if (aula.get("nom").equals(nomiAula)) {
					JSONArray maquines = (JSONArray) aula.get("maquines");
					JSONObject maquiNueva = guardaM.get(0);
					maquines.add(maquiNueva);
					
				}
			}

			FileWriter file = new FileWriter(archiJSON);

			file.write(arrayaulas.toJSONString());
			file.flush();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * habilita en el fitxer json l’aire condicionat de l’aula, si estava
	 * deshabilitat, i deshabilita’l si estava habilitat
	 */
	public void switchAC(String nomAula) {
		try {

			JSONParser parser = new JSONParser();
			Object aulas = parser.parse(new FileReader(archiJSON));
			JSONArray arrayaulas = (JSONArray) aulas;

			for (Object o : arrayaulas) {

				JSONObject aula = (JSONObject) o;
				if (aula.get("nom").equals(nomAula)) {
					if (aula.get("aireacondicionat").equals(true)) {
						aula.put("aireacondicionat", false);
					} else {
						aula.put("aireacondicionat", true);
					}
				}
			}

			FileWriter file = new FileWriter(archiJSON);

			file.write(arrayaulas.toJSONString());
			file.flush();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	 * - Crea un objecte de tipus Classe. Aquest objecte tindrà una aula, un
	 * professor, i una llista de 5 alumnes escollits aleatòriament. S’hauran de
	 * llegir els 3 fitxers per a crear l’objecte. L’objecte es guardarà mitjançant
	 * serialització a “classes.dat”
	 * 
	 */

	public void crearClasse() throws FileNotFoundException, IOException, JAXBException {
		
		ArrayList<String> guardaAulas = new ArrayList<String>();//guardar un aula(la aula 0)
		ArrayList<Alumnes> guardaAlumnos = new ArrayList<Alumnes>();//pillar alumnos

		try {
			JSONParser parser = new JSONParser();
			JSONArray arrysplit = (JSONArray) parser.parse(new FileReader(archiJSON));

			for (Object o : arrysplit) {
				JSONObject split = (JSONObject) o;
				
				guardaAulas.add(split.get("nom").toString());
			}
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		guardaAulas.add(llegirXmlProfes(proftxt).get(0));
		
		String aula = guardaAulas.get(0);
		String profe = llegirXmlProfes(proftxt).get(0);
		//leer alumnos
		File file = new File(archivo);
		JAXBContext jaxbContext = JAXBContext.newInstance(Institut.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Institut ies = (Institut) jaxbUnmarshaller.unmarshal(file);

		while (guardaAlumnos.size()<5) {
			int numIndex;
			numIndex = (int) (Math.random() * ies.getAlumnes().size());
			if (!guardaAlumnos.contains(ies.getAlumnes().get(numIndex))){
				guardaAlumnos.add(ies.getAlumnes().get(numIndex));
			}
		}
		
		Classe DAM = new Classe(aula, profe, guardaAlumnos);
		//System.out.println(DAM.toString());
		
		//SERIALIZAR
		
		try {
			FileOutputStream fileOut = new FileOutputStream("H://DAM//Accès a dades/guardaInsti.dat");//sobreescribiendo, sin el true
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(DAM);
			out.close();
			fileOut.close();
			System.out.println("serializable se ha guardado en: H://DAM//Accès a dades/guardaInsti.dat");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void llegirClasse() {
		
		Classe DAM2 = null;
		
		try {
			FileInputStream fileIn = new FileInputStream("H://DAM//Accès a dades/guardaInsti.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			DAM2 = (Classe) in.readObject();
			in.close();
			fileIn.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("Classe class not found");
			e.printStackTrace();
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		System.out.println("Desserializando de chill...");
		System.out.println("Aula: " + DAM2.aula);
		System.out.println("Profe: " + DAM2.professor);
		System.out.println("Alumnos: " + DAM2.als.toString());
		
		
	}
	
	//MAIN PER PROVAR LA FUNCIONALITAT DEL PROGRAMA
	
	public static void main(String[] args) throws FileNotFoundException, IOException, JAXBException {
		InstitutSabadell iSabadell = new InstitutSabadell();

		//Afegir profe nou
		 iSabadell.afegirProfessor("Carvajal, Carlos");
		 
		// jubilem a Marc Albareda que es massa gran
		 iSabadell.jubilarProfessor("Albareda, David Marc");
		 
		//Dani es un nou alumne
		 Alumnes dani = new Alumnes("Dan", "valiente", "487958I", "calle rules,8",
		 "danvali@gmail.com"); Alumnes dan = new Alumnes("Dan", "valiente",
		 "11111111", "calle rules,8", "danvali@gmail.com");
		 		 
		 iSabadell.afregirAlumne(dani); iSabadell.afregirAlumne(dan);
		 iSabadell.afregirTelefons("487958I", "4554545454554545454545454545454");
		 
		 //Un profe es massa dur i perdem a la meitat de la clase
		 iSabadell.alCarrer("487958I"); 
		 
		 //Nous pc per al aula C4
		 iSabadell.comprarMaquina("C4", "X-X-X", "X",false);
		 //Aquest pc sobra, al aula 1.6
		 iSabadell.canviaMaquina("3-4-1", "1.6");
		 
		 //tenim fred a tenim calor
		 iSabadell.switchAC("C4");
		
		 //clase de Insti nova i guardar a .dat no append
		 iSabadell.crearClasse();
		 
		 //llegir a .dat
		 iSabadell.llegirClasse();
		
		

	}

}
