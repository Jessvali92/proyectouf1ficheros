package dam.jess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Institut implements Serializable{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1014315999903008633L;
	
	private ArrayList<Alumnes> Alumnes = new ArrayList<Alumnes>();
	private String nom;
	
		
	
	@XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElementWrapper(name = "alumnes")
    @XmlElement(name = "alumne")
    public List<Alumnes> getAlumnes() {
        return Alumnes;
    }


	public void setAlumnes(ArrayList<Alumnes> Alumnes) {
		this.Alumnes = Alumnes;
	}
	
	

	

	
}
	


